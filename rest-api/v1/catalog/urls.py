from rest_framework import routers
from django.conf.urls import url
from .views import (
    SectionCatalogViewSet,
    ProductCatalogViewSet,
    VendorCatalogViewSet,
)


router = routers.SimpleRouter()

router.register(r'sections', SectionCatalogViewSet)
router.register(r'products', ProductCatalogViewSet)
router.register(r'vendors', VendorCatalogViewSet)

urlpatterns = router.urls + [
    url(
        r'vendors/(?P<pk>.+)/cfiles/(?P<title>.+)/$',
        VendorCatalogViewSet.as_view({'get': 'cfiles'})
    ),
    url(
        r'products/(?P<pk>.+)/cfiles/(?P<title>.+)/$',
        ProductCatalogViewSet.as_view({'get': 'cfiles'})
    ),
    url(
        r'vendors/(?P<pk>.+)/publications/(?P<title>.+)/$',
        VendorCatalogViewSet.as_view({'get': 'publications'})
    ),
]
