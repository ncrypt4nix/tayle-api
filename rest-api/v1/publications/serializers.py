from django.conf import settings
from django.template import Template, Context
from rest_framework import serializers
from begurucms.pages.models import Publication, PublicationType
from ..core.serializers import BaseSerializer, CFileMixinSerializer


class PublicationTypeSerializer(
    BaseSerializer,
    serializers.HyperlinkedModelSerializer
):
    """Serializer for publication type"""
    class Meta:
        model = PublicationType
        fields = ('id', 'title')


class PublicationBaseSerializer(
    BaseSerializer,
    serializers.HyperlinkedModelSerializer,
    CFileMixinSerializer
):
    """Base serializer for pulications"""
    type = PublicationTypeSerializer(source='ptype')
    url = serializers.SerializerMethodField()

    class Meta:
        model = Publication
        fields = ('id', 'title', 'type', 'url', 'icon', 'published', 'updated')

    def get_url(self, obj):
        """Get full path to absolute url publication"""
        server = ''
        if settings.SERVPREFIX:
            server = settings.SERVPREFIX
        return server + obj.get_absolute_url()


class PublicationSerializer(PublicationBaseSerializer):
    """Serializer for publication"""

    class Meta(PublicationBaseSerializer.Meta):
        fields = PublicationBaseSerializer.Meta.fields + ('annotation', )


class PublicationContentSerializer(serializers.ModelSerializer):
    """Serializer for content publication"""

    content = serializers.SerializerMethodField()

    class Meta:
        model = Publication
        fields = ('content', )


    def get_content(self, obj):
        ttags = r'{% load bgcms_tags %}'
        content = obj.content
        context = Context({'object': obj})
        html = Template('{ttags}{content}'.format(ttags=ttags, content=content)).render(context)
        return html
