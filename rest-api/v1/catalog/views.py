from django.template import Template, Context
from django.http import Http404
from rest_framework import status, viewsets
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.renderers import JSONRenderer
from begurucms.catalog.models import (
    SectionMPTT,
    Product,
    Vendor,
)
from begurucms.core.pagination import ResultAndMaxPagePaginator
from ..events.serializers import EventBaseSerializer
from ..resources.serializers import ResourceBaseSerializer
from ..partners.serializers import QuestionTypeSerializer
from ..publications.serializers import PublicationBaseSerializer
from ..core.serializers import CFileSerializer, DParamSerializer
from ..core.views import PKorIdentMixin, default_action
from .serializers import (
    SectionBaseSerializer,
    SectionCatalogSerializer,
    ProductSectionForProductSerializer,
    ProductCatalogSerializer,
    VendorCatalogSerializer,
    CharSerializer,
    FilterCatalogSerializer,
)


class PKorArtMixin:
    """Get object for pk or articule"""
    def get_object(self, pk=None):
        try:
            return super().get_object()
        except Http404 as e:
            self.kwargs['art'] = self.kwargs['pk']
            self.lookup_field = 'art'
            return super().get_object()


class SectionCatalogViewSet(PKorIdentMixin, viewsets.ReadOnlyModelViewSet):
    """Section ViewSet with data for catalog"""
    queryset = SectionMPTT.objects.filter(ispublished=True)
    serializer_class = SectionCatalogSerializer
    pagination_class = ResultAndMaxPagePaginator

    @action(methods=['get'], detail=True)
    def children(self, request, pk=None):
        """Get all section children"""
        return default_action(
            self,
            self.get_object().get_publish_children(),
            SectionBaseSerializer
        )

    @action(methods=['get'], detail=True)
    def products(self, request, pk=None):
        """Get all section products"""
        return default_action(
            self,
            self.get_object().get_publish_products(),
            ProductSectionForProductSerializer
        )

    @action(methods=['get'], detail=True)
    def filters(self, request, pk=None):
        """Get filters to product into sections"""
        return default_action(
            self,
            self.get_object().get_filters(),
            FilterCatalogSerializer
        )


class ProductCatalogViewSet(PKorArtMixin, viewsets.ReadOnlyModelViewSet):
    """Product ViewSet with data for catalog"""
    queryset = Product.objects.published()
    serializer_class = ProductCatalogSerializer
    pagination_class = ResultAndMaxPagePaginator

    @action(methods=['get'], detail=True)
    def chars(self, request, pk=None):
        """Get all product chars"""
        return default_action(
            self,
            self.get_object().productchar_set.all(),
            CharSerializer
        )

    @action(methods=['get'], detail=True)
    def cfiles(self, request, *args, **kwargs):
        """Get all content files for product"""
        qs = self.get_object().content_files.all()
        if 'title' in kwargs:
            qs = qs.filter(title__iexact=kwargs['title'])
        return default_action(
            self,
            qs,
            CFileSerializer
        )

    @action(methods=['get'], detail=True)
    def resources(self, request, pk=None):
        """Get all product useful resources"""
        return default_action(
            self,
            self.get_object().resource_set.all(),
            ResourceBaseSerializer
        )


class VendorCatalogViewSet(PKorIdentMixin, viewsets.ReadOnlyModelViewSet):
    """Vendor viewset with data for catalog.
    You may get data from ident or id
    """
    queryset = (
        Vendor.objects.published() | Vendor.objects.filter(ident='tayle')
    )
    serializer_class = VendorCatalogSerializer
    pagination_class = ResultAndMaxPagePaginator

    @action(methods=['get'], detail=True)
    def cfiles(self, request, *args, **kwargs):
        """Get all content files for vendor"""
        qs = self.get_object().content_files.all()
        if 'title' in kwargs:
            qs = qs.filter(title__iexact=kwargs['title'])
        return default_action(
            self,
            qs,
            CFileSerializer
        )

    @action(methods=['get'], detail=True)
    def resources(self, request, pk=None):
        """Get all resources for vendor"""
        return default_action(
            self,
            self.get_object().resources(),
            ResourceBaseSerializer
        )

    @action(methods=['get'], detail=True)
    def publications(self, request, *args, **kwargs):
        """Get all resources for vendor"""
        qs = self.get_object().publications()
        if 'title' in kwargs:
            qs = qs.filter(bg_tags__name__iexact=kwargs['title'])
        return default_action(
            self,
            qs,
            PublicationBaseSerializer
        )

    @action(methods=['get'], detail=True)
    def events(self, request, pk=None):
        """Get all resources for vendor"""
        return default_action(
            self,
            self.get_object().scheduled_events(),
            EventBaseSerializer
        )

    @action(methods=['get'], detail=True)
    def description(self, request, pk=None):
        """Get HTML description for vendor"""
        desc = Template(self.get_object().htmldesc).render(Context())
        return Response(desc)

    @action(methods=['get'], detail=True)
    def params(self, request, pk=None):
        """Get vendor dynamic params"""
        return default_action(
            self,
            self.get_object().get_dynamic_params(),
            DParamSerializer
        )

    @action(methods=['get'], detail=True)
    def question_directions(self, request, pk=None):
        """get question direction to contact us form"""
        return default_action(
            self,
            self.get_object().questiontype_set.all(),
            QuestionTypeSerializer
        )
