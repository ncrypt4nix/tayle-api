import requests
from django.conf import settings
from django.contrib.auth import get_user_model
from rest_framework.authentication import TokenAuthentication


class CASTokenAuthentication(TokenAuthentication):
    def authenticate_credentials(self, key):
        user = requests.get(
            '{}/jwt_login/'.format(settings.CAS_SERVER_URL),
            headers={'Authorization': 'Token {}'.format(key)},
            verify=False
        ).json()
        return (None, user)
