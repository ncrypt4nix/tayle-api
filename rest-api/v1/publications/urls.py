from django.conf.urls import url
from rest_framework import routers
from .views import (
    PublicationViewSet,
)


router = routers.SimpleRouter()

router.register(r'publications', PublicationViewSet)

urlpatterns = router.urls + [
    url(
        r'publications/(?P<pk>.+)/content/$',
        PublicationViewSet.as_view({'get': 'content'})
    ),
]