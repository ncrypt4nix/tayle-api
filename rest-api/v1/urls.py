from django.conf.urls import include, url
import os


# Black Magic to create and include urls from dir name
path = os.path.dirname(os.path.realpath(__file__))
dirs = [
    f for f in os.listdir(path)
    if os.path.isdir(os.path.join(path, f)) and
    os.path.isfile(os.path.join(path, f, 'urls.py'))
]

urlpatterns = []
for dir in dirs:
    exec('from .{dir} import urls as {dir}_urls'.format(dir=dir))
    urlpatterns.append(
        url(
            r'{}/'.format(dir),
            include(locals()['{}_urls'.format(dir)])
        )
    )
