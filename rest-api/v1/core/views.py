from rest_framework.views import APIView
from django.http import Http404


class PKorIdentMixin():
    """Get object for pk or ident"""
    def get_object(self, pk=None):
        try:
            return super().get_object()
        except Http404 as e:
            self.kwargs['ident'] = self.kwargs['pk']
            self.lookup_field = 'ident'
            return super().get_object()


def default_action(self, queryset, serializer_class):
    """Default action for GET method with pagination"""
    objects = self.paginate_queryset(queryset)
    serializer = serializer_class(
        data=objects,
        many=True,
        context={'context_object': self.get_object()}
    )
    serializer.is_valid()
    return self.get_paginated_response(serializer.data)
