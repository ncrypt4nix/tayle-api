from rest_framework import serializers
from begurucms.useful_resources.models import Resource, ResourceType
from ..core.serializers import BaseSerializer


class ResourceTypeSerializer(
    BaseSerializer,
    serializers.HyperlinkedModelSerializer,
):
    """Serializer for resource type"""
    class Meta:
        model = ResourceType
        fields = ('id', 'title')


class ResourceBaseSerializer(
    BaseSerializer,
    serializers.HyperlinkedModelSerializer,
):
    """Resources Serializer to json"""
    type = ResourceTypeSerializer(source='resource_type')
    url = serializers.URLField(source='get_absolute_url')

    class Meta:
        model = Resource
        fields = (
            'id',
            'title',
            'type',
            'url'
        )
