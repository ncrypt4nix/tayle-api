from rest_framework import generics
from begurucms.partner_client.models import VendorContactForm
from .serializers import VendorContactFormSerializer


class VendorContactFormAPIView(generics.CreateAPIView):
    """Create question to vendor personal"""
    queryset = VendorContactForm.objects.all()
    serializer_class = VendorContactFormSerializer

    def perform_create(self, serializer):
        obj = serializer.save()
        tpl_name = "question_to_{}".format(obj.vendor.ident)
        obj.send_modeled_email(tpl_name=tpl_name)
        obj.send_modeled_email(tpl_name=tpl_name + "_back", back=True)
