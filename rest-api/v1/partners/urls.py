from rest_framework import routers
from django.conf.urls import url
from .views import VendorContactFormAPIView


router = routers.SimpleRouter()

urlpatterns = router.urls + [
    url(r'^question-to-vendor/$', VendorContactFormAPIView.as_view())
]
