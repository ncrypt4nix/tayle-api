from django.conf.urls import include, url
from rest_framework_swagger.views import get_swagger_view


api_patterns = [
    url(r'v1/', include('rest-api.v1.urls'), name='v1'),
    url(r'v2/', include('rest-api.v2.urls'), name='v2'),
]

schema_view = get_swagger_view(title='Tayle API')

urlpatterns = [
    url(r'^api/', include(api_patterns, namespace='api')),
    url(r'^api/docs/', schema_view)
]
