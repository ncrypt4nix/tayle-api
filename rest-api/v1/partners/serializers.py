from rest_framework import serializers
from begurucms.partner_client.models import QuestionType, VendorContactForm
from ..core.serializers import BaseSerializer, CFileMixinSerializer


class QuestionTypeSerializer(
    BaseSerializer,
    serializers.HyperlinkedModelSerializer,
):
    """Direction question serializer to contact us form"""
    class Meta:
        model = QuestionType
        fields = ('id', 'title')


class VendorContactFormSerializer(
    BaseSerializer,
    serializers.ModelSerializer,
):
    """Serializer for any questions from users to vendor personal"""
    class Meta:
        model = VendorContactForm
        fields = (
            'direction',
            'name',
            'email',
            'phone',
            'company',
            'description',
            'vendor'
        )
