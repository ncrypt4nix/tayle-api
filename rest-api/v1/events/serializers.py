from django.conf import settings
from rest_framework import serializers
from begurucms.events.models import EventPage, EventType
from ..core.serializers import BaseSerializer


class EventTypeSerializer(
    BaseSerializer,
    serializers.HyperlinkedModelSerializer,
):
    """Serializer for event type"""
    class Meta:
        model = EventType
        fields = ('id', 'title')


class EventBaseSerializer(
    BaseSerializer,
    serializers.HyperlinkedModelSerializer,
):
    """Events Serializer to json"""
    type = EventTypeSerializer(source='etype')
    url = serializers.SerializerMethodField()

    class Meta:
        model = EventPage
        fields = (
            'id',
            'title',
            'type',
            'url'
        )

    def get_url(self, obj):
        """Get full path to absolute url publication"""
        server = ''
        if settings.SERVPREFIX:
            server = settings.SERVPREFIX
        return server + obj.get_absolute_url()
