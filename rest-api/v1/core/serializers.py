from django.conf import settings
from rest_framework import serializers
from begurucms.pages.models import PublicationContentFiles, ObjectDynamicParam


class BaseSerializer(serializers.BaseSerializer):
    """Base Serializer for inheritance"""
    def to_representation(self, obj):
        """Replace null data to empty string"""
        data = super().to_representation(obj)
        data = {
            i: data[i]
            if data[i] is not None else ""
            for i in data
        }
        return data


class CFileMixinSerializer(serializers.Serializer):
    """Mixin adding cfile icon field"""
    icon = serializers.SerializerMethodField()

    def get_icon(self, obj):
        """get cfile icon url"""
        server = getattr(settings, 'SERVPREFIX', '')
        try:
            return server + obj.content_files.filter(
                title__iexact='Icon'
            ).first().get_absolute_url()
        except AttributeError:
            return ''


class CFileSerializer(
    BaseSerializer,
    serializers.HyperlinkedModelSerializer
):
    """Serializer for content files"""
    url = serializers.SerializerMethodField()

    class Meta:
        model = PublicationContentFiles
        fields = ('id', 'title', 'url')

    def get_url(self, obj):
        """get content file url with domain"""
        server = getattr(settings, 'SERVPREFIX', '')
        return server + obj.get_absolute_url()


class DParamSerializer(
    BaseSerializer,
    serializers.HyperlinkedModelSerializer
):
    """Dynamic params serializer"""
    title = serializers.SlugRelatedField(
        read_only=True,
        slug_field='title',
        source='dparam'
    )
    ident = serializers.SlugRelatedField(
        read_only=True,
        slug_field='ident',
        source='dparam'
    )
    value = serializers.SerializerMethodField()

    class Meta:
        model = ObjectDynamicParam
        fields = (
            'id',
            'title',
            'ident',
            'value'
        )

    def get_value(self, obj):
        """Get string format value"""
        value =  obj.getParam()
        return str(value) if value is not None else ""
