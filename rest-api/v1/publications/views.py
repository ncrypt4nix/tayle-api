from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import action
from begurucms.core.pagination import ResultAndMaxPagePaginator
from begurucms.pages.models import Publication
from .serializers import (
    PublicationSerializer,
    PublicationContentSerializer
)


class PublicationViewSet(viewsets.ReadOnlyModelViewSet):
    """Publication ViewSet"""
    queryset = Publication.objects.filter(
        ispublished=True,
        accesslevel=0,
        ptype__accesslevel=0
    )
    serializer_class = PublicationSerializer
    pagination_class = ResultAndMaxPagePaginator

    @action(methods=['get'], detail=True)
    def content(self, request, pk=None):
        publication = self.get_object()
        serializer = PublicationContentSerializer(publication)
        return Response(serializer.data)
