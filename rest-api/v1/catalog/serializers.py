from django.template import Template, Context
from rest_framework import serializers
from begurucms.catalog.models import (
    SectionMPTT,
    Product,
    ProductSections,
    ProductChar,
    Vendor
)
from begurucms.catalog__filters.models import Filters, FilterData
from ..core.serializers import BaseSerializer, CFileMixinSerializer


class SectionBaseSerializer(
    BaseSerializer,
    serializers.HyperlinkedModelSerializer,
    CFileMixinSerializer
):
    """Base section serializer"""
    class Meta:
        model = SectionMPTT
        fields = ('id', 'title', 'icon', 'ident')


class ProductBaseSerializer(
    BaseSerializer,
    serializers.HyperlinkedModelSerializer,
    CFileMixinSerializer
):
    """Base product serializer"""
    class Meta:
        model = Product
        fields = (
            'id',
            'art',
            'icon',
        )


class ProductSectionForProductSerializer(
    BaseSerializer,
    serializers.HyperlinkedModelSerializer,
):
    product = ProductBaseSerializer()

    class Meta:
        model = ProductSections
        fields = ('product', )


class VendorBaseSerializer(
    BaseSerializer,
    serializers.HyperlinkedModelSerializer,
    CFileMixinSerializer
):
    """Base serializer for vendors"""
    class Meta:
        model = Vendor
        fields = (
            'id',
            'icon',
            'title',
            'ident',
        )


class ProductCatalogSerializer(ProductBaseSerializer):
    """Product serializer for catalog"""
    content = serializers.SerializerMethodField()

    class Meta(ProductBaseSerializer.Meta):
        fields = ProductBaseSerializer.Meta.fields + (
            'annotation',
            'title',
            'brief',
            'content',
        )

    def get_content(self, obj):
        """render product html content"""
        return Template(
            '{% load bgcms_tags %}' +
            obj.content
        ).render(Context({'product': obj}))


class CharSerializer(
    BaseSerializer,
    serializers.HyperlinkedModelSerializer,
):
    """Char serializer in productChar"""
    name = serializers.CharField(source='char.name')
    value = serializers.CharField()

    class Meta:
        model = ProductChar
        fields = ('name', 'value')


class SectionCatalogSerializer(SectionBaseSerializer):
    """Section serializer for catalog"""
    brief = serializers.SerializerMethodField()

    class Meta(SectionBaseSerializer.Meta):
        fields = SectionBaseSerializer.Meta.fields + (
            'is_leaf_node',
            'brief',
        )

    def get_brief(self, obj):
        """Render section brief"""
        return Template(
            '{% load bgcms_tags %}' +
            obj.brief
        ).render(Context({'section': obj}))


class VendorCatalogSerializer(VendorBaseSerializer):
    """Vendor serializer for catalog"""
    class Meta(VendorBaseSerializer.Meta):
        fields = VendorBaseSerializer.Meta.fields + (
            'email',
            'web',
            'brief',
            'phone',
            'address'
        )


class FilterDataSerializer(
    BaseSerializer,
    serializers.HyperlinkedModelSerializer,
):
    """Filter value serializer"""
    products = serializers.SerializerMethodField()

    class Meta:
        model = FilterData
        fields = ('value', 'products')

    def get_products(self, obj):
        serializer = ProductBaseSerializer(
            obj.products.filter(sections=self.context['context_object']),
            many=True
        )
        return serializer.data


class FilterCatalogSerializer(
    BaseSerializer,
    serializers.HyperlinkedModelSerializer,
):
    """Serializer for filters into catalog"""
    values = FilterDataSerializer(source='data', many=True, read_only=True)

    class Meta:
        model = Filters
        fields = ('title', 'values')
